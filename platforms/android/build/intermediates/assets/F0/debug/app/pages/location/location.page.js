"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var LocationPage = (function () {
    function LocationPage(page) {
        this.page = page;
    }
    LocationPage.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
    };
    LocationPage = __decorate([
        core_1.Component({
            selector: "location",
            templateUrl: "pages/location/location.html",
            styleUrls: ["pages/location/location.css"]
        }), 
        __metadata('design:paramtypes', [page_1.Page])
    ], LocationPage);
    return LocationPage;
}());
exports.LocationPage = LocationPage;
//# sourceMappingURL=location.page.js.map