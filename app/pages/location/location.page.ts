import {Component, OnInit} from "@angular/core";
import {Page} from "ui/page";
import {View} from "ui/core/view";

@Component({
  selector: "location",
  templateUrl: "pages/location/location.html",
  styleUrls: ["pages/location/location.css"]
})

export class LocationPage implements OnInit {

  constructor(private page: Page) {}

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

}