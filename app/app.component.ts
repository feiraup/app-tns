import {Component} from "@angular/core";
import {RouteConfig} from "@angular/router-deprecated";
import {NS_ROUTER_DIRECTIVES, NS_ROUTER_PROVIDERS} from "nativescript-angular/router";

import {LocationPage} from "./pages/location/location.page";

var trace = require("trace");

@Component({
    selector: "main",
    directives: [NS_ROUTER_DIRECTIVES],
    providers: [NS_ROUTER_PROVIDERS],
    templateUrl: "app.html",
})

@RouteConfig([
    {path: "/Location", component: LocationPage, name: "Localização", useAsDefault: true}
])

export class AppComponent {}
